package spring;

import org.apache.camel.CamelContext;
import org.apache.camel.test.spring.CamelSpringRunner;
import org.apache.camel.test.spring.CamelTestContextBootstrapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.BootstrapWith;
import org.springframework.test.context.ContextConfiguration;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;


@RunWith(CamelSpringRunner.class)
@BootstrapWith(CamelTestContextBootstrapper.class)
@ContextConfiguration(locations = "classpath*:camel-spring.xml")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class MySpringApplicationTest {

    @Autowired
    CamelContext camelContext;

    @Test
    public void shouldProduceMessages() throws Exception {

        LinkedList<String> expected = new LinkedList<>();
        expected.add("FIRST");
        expected.add("SECOND");


        Object actual = MySpringApplication.producerTemplate(camelContext).requestBody("direct:should-work", Arrays.asList("FIRST", "SECOND"));


        assertEquals(expected, actual);
    }

    @Test
    public void shouldProduceTheRightMessagesWithHashSet() throws Exception {
        HashSet<String> expected = new HashSet<>();
        expected.add("FIRST");
        expected.add("SECOND");


        Object actual = MySpringApplication.producerTemplate(camelContext).requestBody("direct:shows-the-error", Arrays.asList("FIRST", "SECOND"));


        assertEquals(expected, actual);
    }
}