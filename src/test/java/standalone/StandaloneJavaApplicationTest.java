package standalone;

import org.junit.Test;
import standalone.StandaloneJavaApplication;

import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;

import static org.junit.Assert.assertEquals;

public class StandaloneJavaApplicationTest {

    @Test
    public void shouldProduceMessages() throws Exception {

        LinkedList<String> expected = new LinkedList<>();
        expected.add("FIRST");
        expected.add("SECOND");


        Object actual = StandaloneJavaApplication.getProducer().requestBody("direct:should-work", Arrays.asList("FIRST", "SECOND"));


        assertEquals(expected, actual);
    }

    @Test
    public void shouldProduceTheRightMessagesWithHashSet() throws Exception {
        HashSet<String> expected = new HashSet<>();
        expected.add("FIRST");
        expected.add("SECOND");


        Object actual = StandaloneJavaApplication.getProducer().requestBody("direct:shows-the-error", Arrays.asList("FIRST", "SECOND"));


        assertEquals(expected, actual);
    }

}