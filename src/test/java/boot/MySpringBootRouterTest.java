package boot;

import org.apache.camel.CamelContext;
import org.apache.camel.EndpointInject;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.NotifyBuilder;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = MySpringBootApplication.class, webEnvironment = SpringBootTest.WebEnvironment.NONE)
@EnableAutoConfiguration
public class MySpringBootRouterTest extends Assert {

    @Autowired
    CamelContext camelContext;

    @EndpointInject(uri = "direct:should-work")
    ProducerTemplate producerTemplateShouldWork;

    @EndpointInject(uri = "direct:shows-the-error")
    ProducerTemplate producerTemplateShowsTheError;

    @Test
    public void shouldProduceMessages() throws InterruptedException {
        // we expect that a number of messages is automatic done by the Camel
        // route as it uses a timer to trigger
        NotifyBuilder notify = new NotifyBuilder(camelContext).whenDone(1).and().whenExactlyFailed(0).create();

        producerTemplateShouldWork.sendBody(Arrays.asList("FIRST", "SECOND"));

        assertTrue(notify.matches(10, TimeUnit.SECONDS));
    }

    @Test
    public void shouldProduceTheRightMessagesWithHashSet() throws InterruptedException {
        // we expect that a number of messages is automatic done by the Camel
        // route as it uses a timer to trigger
        HashSet<String> r = new HashSet<>();
        r.add("FIRST");
        r.add("SECOND");

        NotifyBuilder notify = new NotifyBuilder(camelContext)
                .whenDone(1)
                .and()
                .whenExactlyFailed(0)
                .create();

        Set result = producerTemplateShowsTheError.requestBody(Arrays.asList("FIRST", "SECOND"), Set.class);

        assertTrue(notify.matches(10, TimeUnit.SECONDS));
        assertEquals(r, result);
    }

}