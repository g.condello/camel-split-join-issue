package common;

import org.apache.camel.LoggingLevel;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.util.toolbox.AggregationStrategies;

import java.util.HashSet;
import java.util.LinkedList;

public class SampleRouteBuilder extends RouteBuilder {
    @Override
    public void configure() throws Exception {
        defaultErrorHandler().maximumRedeliveries(0);

        from("direct:should-work")
                .id("should-work")
                .log(LoggingLevel.INFO, "Before the first split the body is ${body} and has class ${body.getClass()}")
                .split(body(), AggregationStrategies.flexible().pick(body()).accumulateInCollection(LinkedList.class))
                .log(LoggingLevel.INFO, "During the first split the body is ${body} and has class ${body.getClass()}")
                .end()
                .log(LoggingLevel.INFO, "Before the second split the body is ${body} and has class ${body.getClass()}")
                .split(body(), AggregationStrategies.flexible().pick(body()).accumulateInCollection(LinkedList.class))
                .log(LoggingLevel.INFO, "During the second split the body is ${body} and has class ${body.getClass()}")
                .end()
                .log(LoggingLevel.INFO, "After the second split the body is ${body} and has class ${body.getClass()}")
        ;
        from("direct:shows-the-error")
                .id("shows-the-error")
                .log(LoggingLevel.INFO, "Before the first split the body is ${body} and has class ${body.getClass()}")
                .split(body(), AggregationStrategies.flexible().pick(body()).accumulateInCollection(HashSet.class))
                .log(LoggingLevel.INFO, "During the first split the body is ${body} and has class ${body.getClass()}")
                .end()
                .log(LoggingLevel.INFO, "Before the second split the body is ${body} and has class ${body.getClass()}")
                .split(body(), AggregationStrategies.flexible().pick(body()).accumulateInCollection(HashSet.class))
                .log(LoggingLevel.INFO, "During the second split the body is ${body} and has class ${body.getClass()}")
                .end()
                .log(LoggingLevel.INFO, "After the second split the body is ${body} and has class ${body.getClass()}")
        ;
    }
}
