package standalone;

import common.SampleRouteBuilder;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.impl.DefaultCamelContext;

import java.util.LinkedList;
import java.util.Objects;

public class StandaloneJavaApplication {

    public static void main(String[] args) throws Exception {
        ProducerTemplate template = getProducer();

        for (int i = 0; i < 10; i++) {
            Object r = template.requestBody("direct:should-work", new String[]{"FIRST", "SECOND"});

            Objects.requireNonNull(r);

            if (!(r instanceof LinkedList)) {
                throw new IllegalArgumentException("Result is not a LinkedList but a " + r.getClass() + ": " + r.toString());
            }
        }
    }

    public static ProducerTemplate getProducer() throws Exception {
        CamelContext context = new DefaultCamelContext();
        context.addRoutes(new SampleRouteBuilder());
        ProducerTemplate template = context.createProducerTemplate();
        context.start();
        return template;
    }

}