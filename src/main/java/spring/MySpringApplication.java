package spring;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.LinkedList;
import java.util.Objects;

public class MySpringApplication {

    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = new ClassPathXmlApplicationContext("classpath*:camel-spring.xml");
        ProducerTemplate template = producerTemplate(context.getBean(CamelContext.class));

        for (int i = 0; i < 10; i++) {
            Object r = template.requestBody("direct:should-work", new String[]{"FIRST", "SECOND"});

            Objects.requireNonNull(r);

            if (!(r instanceof LinkedList)) {
                throw new IllegalArgumentException("Result is not a LinkedList but a " + r.getClass() + ": " + r.toString());
            }
        }
    }


    public static ProducerTemplate producerTemplate(CamelContext camelContext) {
        return camelContext.createProducerTemplate();
    }
}
